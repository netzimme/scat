#
# static code analyze testsuite (scat)
# (c) 2014 Daniel Zimmermann
#
#


MAKE=make
ECHO=echo


ifeq ($(OS),Windows_NT)
ifeq ($(MANPATH),)
SCAT_OS=NT
endif
endif


ifeq ($(SCAT_OS),NT)
RM_FILE=del
RM_DIR=rmdir /S /Q
MKDIR=mkdir
FIX_PATH = $(subst /,\,$1)
else
RM_FILE=rm
RM_DIR=rm -rf
MKDIR=mkdir -p
FIX_PATH = $1
endif 



ifeq ($(SCAT_OS),NT)
CLANG_BIN_PATH="C:\\Program Files (x86)\\LLVM\\bin\\clang.exe"
endif 

BUILD_PATH = build_none

ifeq ($(test_program),gcc)
SCAT_TEST_PROGRAM=gcc
SCAT_TEST_PROGRAM_OPTIONS="-c"
SCAT_TEST_PROGRAM2=
SCAT_TEST_PROGRAM_OPTIONS2=
SCAT_TEST_PROGRAM_VERSION=$(SCAT_TEST_PROGRAM) --version
BUILD_PATH = build_gcc
endif

ifeq ($(test_program),clang)
SCAT_TEST_PROGRAM=$(CLANG_BIN_PATH)
SCAT_TEST_PROGRAM_OPTIONS=
SCAT_TEST_PROGRAM2=$(CLANG_BIN_PATH)
SCAT_TEST_PROGRAM_OPTIONS2="--analyze"
SCAT_TEST_PROGRAM_VERSION=$(SCAT_TEST_PROGRAM) --version
BUILD_PATH = build_clang
endif

ifeq ($(test_program),cppcheck)
SCAT_TEST_PROGRAM=cppcheck
SCAT_TEST_PROGRAM_OPTIONS=
SCAT_TEST_PROGRAM2=
SCAT_TEST_PROGRAM_OPTIONS2=
SCAT_TEST_PROGRAM_VERSION=$(SCAT_TEST_PROGRAM) --version
BUILD_PATH = build_cppcheck
endif

ifeq ($(test_program),cl)
SCAT_TEST_PROGRAM=cl
SCAT_TEST_PROGRAM_OPTIONS=/c /nologo
SCAT_TEST_PROGRAM2=cl
SCAT_TEST_PROGRAM_OPTIONS2=/c /analyze /nologo
SCAT_TEST_PROGRAM_VERSION=$(SCAT_TEST_PROGRAM)
BUILD_PATH = build_cl
endif




ifeq ($(test_program),)
ifeq ($(compile_tool),gcc)
SCAT_TEST_PROGRAM=gcc
SCAT_TEST_PROGRAM_OPTIONS=-c
SCAT_TEST_OBJ_EXT=.o
normal_build=y
else
SCAT_TEST_PROGRAM=cl
SCAT_TEST_PROGRAM_OPTIONS=/c
SCAT_TEST_OBJ_EXT=.obj
normal_build=y
endif
else
SCAT_TEST_OBJ_EXT=.o
endif

test_program_version:=$(shell $(SCAT_TEST_PROGRAM_VERSION)) 

MODULES   := buffer_overrun dead_code double_free not_initialized null_pointer_dereference use_after_free memory_leak division_by_zero
SRC_DIR   := $(addprefix src/,$(MODULES))
BUILD_DIR := $(addprefix $(BUILD_PATH)/,$(MODULES))

SRC       := $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.c))
PAT       := $(patsubst src/%.c,$(BUILD_PATH)/%.pat,$(SRC))
OBJ       := $(patsubst src/%.c,$(BUILD_PATH)/%$(SCAT_TEST_OBJ_EXT),$(SRC))

HTML_RESULT_FILE := $(BUILD_PATH)/index.html

$(BUILD_PATH):
	mkdir $(BUILD_PATH)

$(BUILD_PATH)/%.pat: src/%.c
	-$(SCAT_TEST_PROGRAM) $(SCAT_TEST_PROGRAM_OPTIONS) $^ 2> $@
	-$(SCAT_TEST_PROGRAM) $(SCAT_TEST_PROGRAM_OPTIONS) $^ >> $@
ifneq ($(SCAT_TEST_PROGRAM2),)
	-$(SCAT_TEST_PROGRAM2) $(SCAT_TEST_PROGRAM_OPTIONS2) $^ 2>> $@
	-$(SCAT_TEST_PROGRAM2) $(SCAT_TEST_PROGRAM_OPTIONS2) $^ >> $@
endif
	scat_interpret_result /t=$(test_program) $@ "<tr><td>$^</td><td>%%s</td></tr>" >> $(HTML_RESULT_FILE)


$(BUILD_PATH)/%$(SCAT_TEST_OBJ_EXT): src/%.c
ifeq ($(compile_tool),gcc)
	$(SCAT_TEST_PROGRAM) $(SCAT_TEST_PROGRAM_OPTIONS) $^ -o$@
else
	$(SCAT_TEST_PROGRAM) $(SCAT_TEST_PROGRAM_OPTIONS) $^ /Fo$@
endif




$(HTML_RESULT_FILE):
	scat_interpret_result /t=echo "dummy" "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html><head><title>scat result</title></head><body><h1>scat result for $(test_program_version)</h1><table border="1">" > $(HTML_RESULT_FILE)

test_modules_begin: $(HTML_RESULT_FILE)

test_modules_build: $(PAT)

test_modules_end:
	scat_interpret_result /t=echo "dummy" "</table></body></html>" >> $(HTML_RESULT_FILE)


test_modules:
	$(MAKE) test_modules_begin
	$(MAKE) test_modules_build
	$(MAKE) test_modules_end

build_modules: $(OBJ)

all: checkdirs
ifeq ($(normal_build),y)
	$(MAKE) build_modules
ifeq ($(compile_tool),gcc)
	$(SCAT_TEST_PROGRAM) $(SCAT_TEST_PROGRAM_OPTIONS) src/scat_main.c -o$(BUILD_PATH)/scat_main.o
	$(SCAT_TEST_PROGRAM) $(OBJ) $(BUILD_PATH)/scat_main.o -o$(BUILD_PATH)/scat_main.exe
else
	$(SCAT_TEST_PROGRAM) $(OBJ) src/scat_main.c /Fo$(BUILD_PATH)/scat_main.obj /Fe$(BUILD_PATH)/scat_main.exe
endif
else
	$(MAKE) test_modules
endif


checkdirs: $(BUILD_DIR)

$(BUILD_DIR): clean 
	$(MKDIR) $(call FIX_PATH,$@)


build_interpret_result:
ifeq ($(compile_tool),cl)
	$(SCAT_TEST_PROGRAM) src/scat_interpret_result.c /Foscat_interpret_result.obj /Fescat_interpret_result.exe
endif

clean:
	-$(RM_DIR) $(BUILD_PATH)

help:
	@echo "make <options> <targets>"
	@echo ""
	@echo "<targets>"
	@echo "             all"
	@echo "             clean"
	@echo "<options>:"
	@echo "             compile_tool=gcc|cl"
	@echo "             test_program=gcc|cl|cppcheck"
	@echo ""
	@echo "examples"
	@echo "make all test_program=cl"
	@echo "make all test_program=cppcheck"
	@echo ""
	@echo "Variables:
	@echo "OS:         $(OS)"
	@echo "MANPATH:    $(MANPATH)"
	@echo "SRC DIR:    ${SRC_DIR}"
    @echo "Build DIR:  ${BUILD_DIR}"
    @echo "Source:     ${SRC}"
	@echo "Pat:        ${PAT}"

.PHONY: all clean
