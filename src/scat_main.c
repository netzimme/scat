/*
   static code analyze testsuite
   scat_main.c
   (c) 2014 Daniel Zimmermann
*/

#include <stdio.h>

#include "buffer_overrun\test_modules.h"
#include "dead_code\test_modules.h"
#include "double_free\test_modules.h"
#include "not_initialized\test_modules.h"
#include "null_pointer_dereference\test_modules.h"
#include "use_after_free\test_modules.h"
#include "memory_leak\test_modules.h"
#include "division_by_zero\test_modules.h"


/*
   build a executable of all testsuites.
*/
int main(int argc, char* argv[])
{
    int i;

    i = 0;

    i += buffer_overrun_test_1();
    i += buffer_overrun_test_2();
    i += buffer_overrun_test_3();
    i += buffer_overrun_test_4();
    i += buffer_overrun_test_5();
    i += buffer_overrun_test_6();
    i += buffer_overrun_test_7();
    i += dead_code_test_1();
    i += double_free_test_1();
    i += not_initialized_test_1();
    i += not_initialized_test_2();
    i += not_initialized_test_3();
    i += null_pointer_dereference_test_1();
    i += use_after_free_test_1();
    i += use_after_free_test_2();
    i += use_after_free_test_3();
    i += memory_leak_test_1();
    i += memory_leak_test_2();
    i += division_by_zero_1();
    i += division_by_zero_2();

    return i;
}
