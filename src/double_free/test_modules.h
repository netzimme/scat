/*
   static code analyze testsuite
   double free tests
   test_modules.h
*/

#ifndef _DOUBLE_FREE_H_
#define _DOUBLE_FREE_H_

int double_free_test_1(void);

#endif // _DOUBLE_FREE_H_

