/*
   static code analyze testsuite
   double free test 1
   test_module1.c
*/

#include <stdio.h>
#include <stdlib.h>


int double_free_test_1_int(char* p)
{
   int r;


   if (p)
   {
      p[0] = 1;
      p[1] = 99;
      r = p[0] + p[1];
      free(p);
   }
   else
   {
      r = 0;
   }
   return r;
}

int double_free_test_1(void)
{
   int   r;
   char* p1;
   char* p2;

   r = 55;

   p1 = malloc(10);
   p2 = malloc(10);


   if (p1)
   {
      r = double_free_test_1_int(p2);
      free(p1);
      if (r>0)
      { 
         free(p2);
      }
   }
   return r;
}
