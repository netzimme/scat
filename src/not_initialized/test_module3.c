/*
   static code analyze testsuite
   not initialized test 2
   test_module2.c
*/

#include <stdio.h>


int not_initialized_test_3_int(int* pnotinit)
{
   int r;

   r = 9;
   if (pnotinit)
   {
      *pnotinit = 42;/*pointer uninit.*/
      r = r + 3;
   }
   return r;
}


int not_initialized_test_3(void)
{
   int* pnotinit;

   return not_initialized_test_3_int(pnotinit);
}

