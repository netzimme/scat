/*
   static code analyze testsuite
   not initialized test 2
   test_module2.c
*/

#include <stdio.h>

int not_initialized_test_2_int(int k)
{
   int   iret;
   int*  pc1;

   *pc1 = 5;
   /* not initialized */
   return k+*pc1;
}


int not_initialized_test_2(void)
{
   return not_initialized_test_2_int(3) + not_initialized_test_2_int(45);
}

