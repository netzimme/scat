/*
   static code analyze testsuite
   not initialized test 1
   test_module1.c
*/

#include <stdio.h>

int not_initialized_test_1_int(int k)
{
   int   iret;

   if (k > 42)
   {
      iret = 3;
   }
   /* not initialized */
   return iret;
}


int not_initialized_test_1(void)
{
   return not_initialized_test_1_int(3) + not_initialized_test_1_int(45);
}

