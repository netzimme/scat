/*
   static code analyze testsuite
   not initialized tests
   test_modules.h
*/

#ifndef _NOT_INITIALIZED_H_
#define _NOT_INITIALIZED_H_

int not_initialized_test_1(void);
int not_initialized_test_2(void);
int not_initialized_test_3(void);

#endif // _NOT_INITIALIZED_H_

