/*
   static code analyze testsuite
   division by zero test 1
   test_module1.c
*/

#include <stdio.h>


int division_by_zero_1(void)
{
   int i;

   i = 0;

   return printf("%i", (int)((10) / i));
}
