/*
   static code analyze testsuite
   division by zero test
   test_modules.h
*/

#ifndef _DIVISION_BY_ZERO_H_
#define _DIVISION_BY_ZERO_H_

int division_by_zero_1(void);
int division_by_zero_2(void);

#endif // _DIVISION_BY_ZERO_H_

