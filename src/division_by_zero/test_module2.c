/*
   static code analyze testsuite
   division by zero test 2
   test_module2.c
*/

#include <stdio.h>


int division_by_zero_2_some_fun1(int a)
{
   return 10 / a;
}

int division_by_zero_2_some_fun2(int b)
{
   if (b == -1)
   {
      return 0;
   }
   return division_by_zero_2_some_fun1(b) + division_by_zero_2_some_fun2(b - 1);
}


int division_by_zero_2(void)
{
   return printf("%i\n", division_by_zero_2_some_fun2(4));
}
