/*
   static code analyze testsuite
   buffer overrun tests
   test_modules.h
*/

#ifndef _BUFFER_OVERRUN_H_
#define _BUFFER_OVERRUN_H_

int buffer_overrun_test_1(void);
int buffer_overrun_test_2(void);
int buffer_overrun_test_3(void);
int buffer_overrun_test_4(void);
int buffer_overrun_test_5(void);
int buffer_overrun_test_6(void);
int buffer_overrun_test_7(void);

#endif // _BUFFER_OVERRUN_H_

