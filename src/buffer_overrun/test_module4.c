/*
   static code analyze testsuite
   buffer overrun test 4
   test_module4.c
*/


int buffer_overrun_test_4(void)
{
   int  i;
   int  j;
   int  buffer[10];
   int* pc;

   j = 0;
   for (i = 0; i < 6; i++)
   {
      buffer[j++] = 'd';
   }
   
   for (i = 0; i < 6; i++)
   {
      /* buffer overrun */
      buffer[j++] = 'e';
   }

   return buffer[0];
}



