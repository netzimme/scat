/*
   static code analyze testsuite
   buffer overrun test 6
   test_module6.c
*/

#include <stdlib.h>

int buffer_overrun_test_6_int(int* p)
{
   int * pdum;
   if (p)
   {
      p[0] = 6;
      pdum = p + 1;
      /* buffer overrun */
      *(pdum) = 42;
   }

   return p[0];
}


int buffer_overrun_test_6(void)
{
   int  r;
   int* p;

   r = 0;
   p = (int*)malloc(1 * sizeof(int));
   if (p)
   {
      p[0] = 2;
      r = buffer_overrun_test_6_int(p);
      free(p);
   }
   return r;
}
