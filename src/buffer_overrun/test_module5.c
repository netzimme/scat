/*
   static code analyze testsuite
   buffer overrun test 5
   test_module5.c
*/


int buffer_overrun_test_5_int(int test[])
{
   /* no buffer overrun */
   test[3] = 4;
   /*buffer overrun */
   test[4] = 5;

   return test[3];
}


int buffer_overrun_test_5(void)
{
   int test[4];

   return buffer_overrun_test_5_int(test);
}



