/*
   static code analyze testsuite
   buffer overrun test 3
   test_module3.c
*/

int buffer_overrun_test_3(void)
{
   int  i;
   int  buffer[10];
   int* pc;

   pc = &buffer[0];
   for (i = 0; i <= 10; i++)
   {
      /* buffer overrun */
      *pc++ = 'c';
   }

   return buffer[0];
}



