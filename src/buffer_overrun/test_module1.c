/*
   static code analyze testsuite
   buffer overrun test 1
   test_module1.c
*/

int buffer_overrun_test_1(void)
{
   int buffer[10];

   /* no buffer overrun */
   buffer[0] = 'a';
   
   /* buffer overrun */
   buffer[10] = 'a';

   return buffer[0];
}


