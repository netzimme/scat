/*
   static code analyze testsuite
   buffer overrun test 7
   test_module7.c
*/

#include <stdio.h>
#include <stdlib.h>

int buffer_overrun_test_7(void)
{
   int i;
   int a[] = { 1, 2, 3 };
   int n = sizeof(a) / sizeof(int);
   
   for (i = 0; i <= n; i++)
   {
      printf("a[%d]=%d\n", i, a[i]);
   }
   return 3;
}
