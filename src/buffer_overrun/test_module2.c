/*
   static code analyze testsuite
   buffer overrun test 2
   test_module2.c
*/

int buffer_overrun_test_2(void)
{
   int i;
   int buffer[10];

   for (i = 0; i <= 10; i++)
   {
      /* buffer overrun */
      buffer[i] = 'c';
   }

   return buffer[0];
}

