/*
   static code analyze testsuite
   use after free test 3
   test_module3.c
*/

#include <stdio.h>
#include <stdlib.h> 

int use_after_free_test_3_fun(int i)
{
   return i * i;
}

int use_after_free_test_3_int(int i)
{
   char* pc1;
   char* pc2;
   int r;

   r = i;

   pc1 = (char*)malloc(10 * sizeof(char));
   if (pc1)
   {
      pc2 = pc1 + use_after_free_test_3_fun(r);
      if (pc2)
      {
         free(pc2 - use_after_free_test_3_fun(i));
         pc1[2] = 'h';
         r = 5;
      }
      else
      {
         free(pc1);
         r = 2;
      }
   }
  
   return r;
}

int use_after_free_test_3(void)
{
   return use_after_free_test_3_int(2);
}

