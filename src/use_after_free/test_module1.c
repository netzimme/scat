/*
   static code analyze testsuite
   use after free test 1
   test_module1.c
*/

#include <stdio.h>
#include <stdlib.h> 

int use_after_free_test_1_int()
{
   char* pc1;
   int r;

   r = 0;

   pc1 = (char*)malloc(10 * sizeof(char));
   if (pc1)
   {
      pc1[0] = 'a';
      r = (int)pc1[0];
      free(pc1);
      /* use after free */
      pc1[1] = 'b';
   }
  
   return r;
}

int use_after_free_test_1(void)
{
   return use_after_free_test_1_int();
}

