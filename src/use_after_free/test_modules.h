/*
   static code analyze testsuite
   use_after_free tests
   test_modules.h
*/

#ifndef _USE_AFTER_FREE_H_
#define _USE_AFTER_FREE_H_

int use_after_free_test_1(void);
int use_after_free_test_2(void);
int use_after_free_test_3(void);

#endif // _USE_AFTER_FREE_H_

