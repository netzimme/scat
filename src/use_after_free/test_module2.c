/*
   static code analyze testsuite
   use after free test 2
   test_module2.c
*/

#include <stdio.h>
#include <stdlib.h> 

int use_after_free_test_2_int()
{
   char* pc1;
   char* pc2;
   int r;

   r = 0;

   pc1 = (char*)malloc(10 * sizeof(char));
   if (pc1)
   {
      pc2 = pc1;
      pc1[0] = 'a';
      r = (int)pc2[0];
      free(pc2);
      /* use after free */
      pc2[1] = 'b';
   }
  
   return r;
}

int use_after_free_test_2(void)
{
   return use_after_free_test_2_int();
}

