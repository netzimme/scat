/*
   static code analyze testsuite
   dead code test 1
   test_module1.c
*/

#include <stdio.h>

int dead_code_test_1_some_func(void)
{
   return fprintf(stderr, "code");;
}

int dead_code_test_1_some_other_func(void)
{
   return fprintf(stderr, "dead code!!!");
}

int dead_code_test_1(void)
{
   int x = dead_code_test_1_some_func();
   int y = dead_code_test_1_some_other_func();
   if (x)
   {
      if (y>10 && !x)
      {
         x++; /* dead code*/
         return 2;
      }
   }
   return 1;
}
