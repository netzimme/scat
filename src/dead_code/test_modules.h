/*
   static code analyze testsuite
   dead code tests
   test_modules.h
*/

#ifndef _DEAD_CODE_H_
#define _DEAD_CODE_H_

int dead_code_test_1(void);

#endif // _DEAD_CODE_H_

