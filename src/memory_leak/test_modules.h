/*
   static code analyze testsuite
   memory leak tests
   test_modules.h
*/

#ifndef _MEMORY_LEAK_H_
#define _MEMORY_LEAK_H_

int memory_leak_test_1(void);
int memory_leak_test_2(void);

#endif // _MEMORY_LEAK_H_

