/*
   static code analyze testsuite
   memory leak test 2
   test_module2.c
*/

#include <stdio.h>
#include <stdlib.h>


int memory_leak_test_2_int(int *p, int x)
{
   
   /*memory leak*/
   if (p && (p[5]+x) < 10) 
   {
      free(p);
   }
   return x;
}


int memory_leak_test_2(void)
{
   int *p1;

   p1 = (int*)malloc(10 * sizeof(int));
   if (p1)
   {
      p1[5] = 5;
   }
   return memory_leak_test_2_int(p1, 5);
}

