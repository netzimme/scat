/*
   static code analyze testsuite
   memory leak test 1
   test_module1.c
*/

#include <stdio.h>
#include <stdlib.h> 
#include <time.h> 


int memory_leak_test_1_some_func(void)
{
   return rand();
}



int memory_leak_test_1_int()
{
   char *p = (char*)malloc(12);
   if (!p)
   {
      return 0;
   }
   if (!memory_leak_test_1_some_func())
   {
      free(p);
      return -1;
   }
   if (!memory_leak_test_1_some_func())
   {
      /* memory leak */
      return -2;
   }
   free(p);
   return 1;
}

int memory_leak_test_1(void)
{
   srand((unsigned)time(NULL));
   return memory_leak_test_1_int();
}

