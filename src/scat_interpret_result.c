/*
   static code analyze testsuite
   scat_interpret_result.c
   (c) 2014 Daniel Zimmermann
*/


#define  _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OUTPUT_STREAM stdout


typedef struct 
{
   char  inputfilename[_MAX_PATH];
   char* inputfile_buffer;
   FILE* outputfile;
   char* outputpattern;
   int   result;
}scat_ir_type;


/*
   interpret the result of the differenc analyze tools
*/


int scat_ir_help(void)
{
   printf("scat_interpret_resul /t=[cppcheck|cl|clang] <pattern file>\n");
   return 0;
}

int scan_ir_get_result_pattern(scat_ir_type* scat_ir_context, char* result_pattern)
{
   if (scat_ir_context->result == 1)
   {
      return sprintf(result_pattern, "Y");
   }
   return sprintf(result_pattern, "N");
}


int scat_ir_print_result(scat_ir_type* scat_ir_context)
{
   char result_pattern[256];

   scan_ir_get_result_pattern(scat_ir_context, result_pattern);

   if (scat_ir_context->outputpattern != NULL)
   {
      fprintf(scat_ir_context->outputfile, scat_ir_context->outputpattern, result_pattern);
   }
   else
   {
      fprintf(scat_ir_context->outputfile, result_pattern);
   }
   return 0;
}


int scat_ir_cppcheck(scat_ir_type* scat_ir_context)
{
   if (strstr(scat_ir_context->inputfile_buffer, ": (error) ") != NULL)
   {
      scat_ir_context->result = 1;
   }
   else
   {
      scat_ir_context->result = 0;
   }
      
   return scat_ir_print_result(scat_ir_context);
}

int scat_ir_cl(scat_ir_type* scat_ir_context)
{
   if (strstr(scat_ir_context->inputfile_buffer, " : warning C") != NULL)
   {
      scat_ir_context->result = 1;
   }
   else
   {
      scat_ir_context->result = 0;
   }

   return scat_ir_print_result(scat_ir_context);
}

int scat_ir_clang(scat_ir_type* scat_ir_context)
{
   if (strstr(scat_ir_context->inputfile_buffer, ": warning: ") != NULL)
   {
      scat_ir_context->result = 1;
   }
   else
   {
      scat_ir_context->result = 0;
   }

   return scat_ir_print_result(scat_ir_context);
}

int scat_ir_echo(scat_ir_type* scat_ir_context)
{
   scat_ir_context->result = 1;
   return scat_ir_print_result(scat_ir_context);
}

int main(int argc, char* argv[])
{
   int     check_parameter;
   FILE*   inputfile;
   char*   inputfile_buffer;
   size_t  inputfile_length;
   int     result;
   scat_ir_type scat_ir_context;

   result = 0;
   check_parameter = 0;
   
   if (argc >= 3)
   {
      memset(&scat_ir_context, 0, sizeof(scat_ir_context));
      if (strcmp(argv[1], "/t=echo") == 0)
      {
         check_parameter = 1;
         scat_ir_context.outputfile = OUTPUT_STREAM;
         if (argv[3] && argv[3][0] != 0)
         {
            scat_ir_context.outputpattern = _strdup(argv[3]);
         }
         result = scat_ir_echo(&scat_ir_context);
      }
      else
      {
         if (argv[2][0] != 0)
         {
            inputfile = fopen(argv[2], "rb");
            if (inputfile != NULL)
            {
               inputfile_buffer = NULL;
               fseek(inputfile, 0, SEEK_END);
               inputfile_length = ftell(inputfile);
               fseek(inputfile, 0, SEEK_SET);
               inputfile_buffer = malloc(inputfile_length + 1);
               if (inputfile_buffer != NULL)
               {
                  if (fread(inputfile_buffer, 1, inputfile_length, inputfile) == inputfile_length)
                  {
                     inputfile_buffer[inputfile_length] = 0;

                     if (argv[3] && argv[3][0] != 0)
                     {
                        scat_ir_context.outputpattern = _strdup(argv[3]);
                     }
                     else
                     {
                        scat_ir_context.outputpattern = NULL;
                     }


                     strncpy(scat_ir_context.inputfilename, argv[2], sizeof(scat_ir_context.inputfilename) - 1);
                     scat_ir_context.outputfile = OUTPUT_STREAM;
                     scat_ir_context.inputfile_buffer = inputfile_buffer;
                     if (strcmp(argv[1], "/t=cppcheck") == 0)
                     {
                        check_parameter = 1;
                        result = scat_ir_cppcheck(&scat_ir_context);
                     }
                     if (strcmp(argv[1], "/t=cl") == 0)
                     {
                        check_parameter = 1;
                        result = scat_ir_cl(&scat_ir_context);
                     }
                     if (strcmp(argv[1], "/t=clang") == 0)
                     {
                        check_parameter = 1;
                        result = scat_ir_clang(&scat_ir_context);
                     }
                  }
                  free(inputfile_buffer);
               }
               fclose(inputfile);
            }
         }
      }
   }
   
   if (check_parameter == 0)
   {
      scat_ir_help();
   }
  
   return result;
}
