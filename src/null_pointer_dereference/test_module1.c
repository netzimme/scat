/*
   static code analyze testsuite
   null pointer dereference test 1
   test_module1.c
*/

#include <stdio.h>


int null_pointer_dereference_test_1_int(int* pnull)
{
   int r;

   r = 5;
   
   return *pnull * r;
}


int null_pointer_dereference_test_1(void)
{
   int* pnull;

   pnull  = NULL;

   return null_pointer_dereference_test_1_int(pnull);
}

