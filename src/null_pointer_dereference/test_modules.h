/*
   static code analyze testsuite
   null pointer dereference tests
   test_modules.h
*/

#ifndef _NULL_POINTER_DEREFERENCE_H_
#define _NULL_POINTER_DEREFERENCE_H_

int null_pointer_dereference_test_1(void);

#endif // _NULL_POINTER_DEREFERENCE_H_

